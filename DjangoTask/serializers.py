from .models import Stationary
from rest_marshmallow import Schema, fields, ValidationError


class StationarySerializer(Schema):
    # class Meta:
    #     model = Stationary
    #     fields = ['id', 'status', 'item']
    id = fields.Integer()
    status = fields.String()
    item = fields.String()

    def create(self, validated_data):
        value = validated_data['item']
        print (value)
        valid_list = ['book', 'pen', 'folder', 'bag']
        if value.lower() not in valid_list:
            raise ValidationError({'msg':'Invalid Item'})
        return Stationary.objects.create(**validated_data)



