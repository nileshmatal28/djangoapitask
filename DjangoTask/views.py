from django.shortcuts import render
# from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Stationary
from .serializers import StationarySerializer
from rest_framework import  status


# Create your views here.
# Question 1 :
class StationaryApi(APIView):

    def post(self, request, format=None):
        serializer = StationarySerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors)


# Question 2:
from rest_framework import generics
import requests
import time
import concurrent.futures

delay_value = 0;

# Fire after API Call


class DelayValue(generics.ListAPIView):

    def get(self, format=None):
        start = time.perf_counter()
        try:
            delay = int(self.request.query_params.get('delay_value'))
            if delay is not None:
                with concurrent.futures.ThreadPoolExecutor() as executor:
                    results = [executor.submit(get_delay, delay) for _ in range(5)]

                finish = time.perf_counter()
                time_taken = finish-start
                print(time_taken)
                msg = {"time_taken": round(time_taken, 2)}
                return Response(msg)
        except ValueError:
            msg = {"error": "That's not an int!"}
            return Response(msg)

# get delay value from query params


def get_delay(delay_value):
    url = "https://httpbin.org/delay/" + str(delay_value)
    print('started', url)
    requests.get(url=url)
    print('Completed')



