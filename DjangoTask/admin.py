from django.contrib import admin
from .models import Stationary

# Register your models here.
@admin.register(Stationary)
class StationaryDetails(admin.ModelAdmin):
    list_display = ['id', 'status', 'item']
