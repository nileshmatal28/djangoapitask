import time
import threading
import concurrent.futures
import requests

URL ="https://httpbin.org/delay/2"

delay_value = 4
str = "https://httpbin.org/delay/"+str(delay_value)
print(str)

start = time.perf_counter()
def get_delaytime():
    print('started')
    requests.get(url = URL)
    return 'Completed'

with concurrent.futures.ThreadPoolExecutor() as executor:
    results = [executor.submit(get_delaytime) for _ in range(50)]

    for f in concurrent.futures.as_completed(results):
        print(f.result())

finish = time.perf_counter()
print(f'Finished in {round(finish-start, 2)} second(s)')

